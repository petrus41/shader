using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Shape 
{
    protected ColorBridge Color;
    protected int NumberOfFaces;
    public abstract void Stretch();
    public abstract void Shine();
    public abstract void FadeToBlack();
}

public class Cube : Shape
{
    public Cube (int NumberOfFaces, ColorBridge Color)
    {
        this.Color = Color;
        this.NumberOfFaces = NumberOfFaces;
    }

    public override void FadeToBlack()
    {
       
    }

    public override void Shine()
    {
        
    }

    public override void Stretch()
    {
       
    }
}
public class Sphere : Shape
{
    public Sphere(int NumberOfFaces, ColorBridge Color)
    {
        this.Color = Color;
        this.NumberOfFaces = NumberOfFaces;
    }

    public override void FadeToBlack()
    {
        
    }

    public override void Shine()
    {
       
    }

    public override void Stretch()
    {
       
    }
}