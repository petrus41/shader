using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBridge 
{
    public ColorBridge(Vector3 RGB)
    {
        this.RGB = RGB;
    }
    protected Vector3 RGB;
    public Vector3 GetRGBValue() => RGB;
    public void SetRGBValue(Vector3 RGB) => this.RGB = RGB;
}
public class Red : ColorBridge
{
    public Red(Vector3 RGB) : base(RGB)
    {
    
    }

}

public class Blue : ColorBridge
{
    public Blue(Vector3 RGB) : base(RGB)
    {
       
    }

}
