Shader "Unlit/Texture"
{
    Properties
    {
        _BorderColor("Main Color", Color) = (0,0,1,1)
        _CenterColor("Main Color", Color) = (0,0,1,1)
        _CenterTex("Texture", 2D) = "white" {}
        _BorderTex("Texture", 2D) = "white" {}
    }
        SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _CenterTex;
            sampler2D _BorderTex;
            fixed4 _BorderColor;
            fixed4 _CenterColor;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_CenterTex, i.uv)* _CenterColor + tex2D(_BorderTex, i.uv) * _BorderColor;
                return col;
            }
         ENDCG
        }
    }
}