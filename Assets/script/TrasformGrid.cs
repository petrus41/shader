using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrasformGrid : MonoBehaviour
{
    [SerializeField]
    GameObject Prefab;
    [SerializeField]
    Vector3 ScaleVector= new Vector3(1,1,1);
    [SerializeField]
    Vector3 TranslationVector;
    List<GameObject> Grid = new List<GameObject>();

    //[SerializeField]
    float GridDimansion = 20f;

    Matrix4x4 TranslationMatrix;
    // Start is called before the first frame update
    void Start()
    {
        for (int x = 0; x < GridDimansion; x++)
        {
            for (int y = 0; y < GridDimansion; y++)
            {
                for (int z = 0; z < GridDimansion; z++)
                {
                    CreateObject(new Vector3(x, y, z));
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        ApplyTransform();
    }

    private void CreateObject(Vector3 position)
    {
        GameObject temp = Instantiate(Prefab, position, Quaternion.identity);
        MeshRenderer Renderer = temp.GetComponent<MeshRenderer>();
        Grid.Add(temp);
        Renderer.material.color = new Color(position.x / GridDimansion, position.y / GridDimansion, position.z / GridDimansion);
    }

    private void ApplyTransform()
    {
        TranslationMatrix.SetRow(0, new Vector4(1,0,0,TranslationVector.x));
        TranslationMatrix.SetRow(0, new Vector4(0,1,0,TranslationVector.y));
        TranslationMatrix.SetRow(0, new Vector4(0,0,1,TranslationVector.z));
        TranslationMatrix.SetRow(0, new Vector4(0,0,0,1));

        foreach(GameObject Object in Grid)
        {
           Object.transform.position = TranslationMatrix.MultiplyPoint3x4(Object.transform.position);
        }
    }
}
