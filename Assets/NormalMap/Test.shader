// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/NormalMapShader"
{
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_NormalMap("NormalMap", 2D) = "white" {}
		_MainLightPosition("MainLightPosition", Vector) = (0,0,0,0)
	}
		SubShader
		{
			Pass
			{
				CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				struct appdata
				{
					float4 position : POSITION;
					float3 normal : NORMAL;
					float3 tangent : TANGENT;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float3 lightdir : TEXCOORD1;
					float3 Tangent : TEXCOORD2;
					float3 Binormal : TEXCOORD3;
					float3 Normal : TEXCOORD4;
				};

				sampler _MainTex;
				sampler _NormalMap;
				float3 _MainLightPosition;

				v2f vert(appdata input)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(input.position);
					o.uv = input.uv;

					// calc lightDir vector 
					float4 worldPosition = mul(unity_ObjectToWorld, input.position);
					float3 lightDir = worldPosition.xyz - _MainLightPosition.xyz;
					o.lightdir = normalize(lightDir);

					// Matrice cambio base per worldSpace
					float3 worldNormal = mul((float3x3)unity_ObjectToWorld, input.normal);
					float3 worldTangent = mul((float3x3)unity_ObjectToWorld, input.tangent);
					float3 binormal = cross(input.normal, input.tangent.xyz); 
					float3 worldBinormal = mul((float3x3)unity_ObjectToWorld, binormal);
					o.Normal = normalize(worldNormal);
					o.Tangent = normalize(worldTangent);
					o.Binormal = normalize(worldBinormal);

					return o;
				}

				float4 frag(v2f input) : COLOR
				{
					// normale calcolata dalla Map in worldSpace
					float3 Normal = tex2D(_NormalMap, input.uv).xyz;
					// i valori sono (0 ; 1) diventano (-1 ; 1)
					Normal = normalize(Normal * 2 - 1);

					//MatriceCambioBase World To Tangent Space
					float3x3 TBN = float3x3(normalize(input.Tangent), normalize(input.Binormal), normalize(input.Normal));
					TBN = transpose(TBN);

					// vettore normale cambiato di base
					float3 worldNormal = mul(TBN, Normal);

					// Colore base
					float3 lightDir = normalize(input.lightdir);
					float4 albedo = tex2D(_MainTex, input.uv);
					float3 diffuse = saturate(dot(worldNormal, -lightDir));
					diffuse = albedo.rgb * diffuse;
					return float4( diffuse  , 1);
				}
				ENDCG
			}
		}
			FallBack "Diffuse"
}